const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

//middleware
app.use(express.json());

// MongoDB connection
//mongoose.connect('mongodb://poli:password@localhost:27017/recipes?authSource=admin')
mongoose.connect('mongodb://poli:password@mongoRecetas:27017/recipes?authSource=admin')
.then(() => console.log('Conectado a Mongo'))
.catch((error) => console.error(error))

app.get('/', (reg, res) => {
    res.send('Hola Mundo');
})

app.post('/new_user', (req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/rate', (req,res) => {
    const { recipeId, userId, rating } = req.body
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    )
    .then((data) => res.json(data))
    .catch((error) => {
        console.log(error)
        res.send(error)
    });
})


app.get('/recipes', (req,res) => {
    const {userId, recipeId} = req.body
    if (recipeId){
        recipeSchema
            .find( { _id : recipeId } )
            .then((data) => res.json(data))
            .catch((error) => res.json({message: error}))
    } else {
        recipeSchema
            .find( {userId: userId} )
            .then((data) => res.json(data))
            .catch((error) => res.json({message: error}))
    }
})


app.get('/recipesbyingredient', (req,res) => {
    const {ingredients} = req.body
    const ingredientsArray = ingredients.map(a=>a.name)
    console.log('ingredientsArray')
    console.log(ingredientsArray)
    recipeSchema
        .find({$expr:{$setIsSubset: ["$ingredients.name",ingredientsArray]}})
        .then((data) => {
            console.log('Se listan las recetas con los ingredientes solicitados')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))
})


app.listen(3000, () => console.log("Esperando en puerto 3000"));